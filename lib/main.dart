import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main(){
  runApp(myapp());
}
class myapp extends StatelessWidget {
  const myapp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
           crossAxisAlignment: CrossAxisAlignment.center,
           children: [
            Container(
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                child: Image.asset("assrts/images/images.jfif", height: 250, fit: BoxFit.fitHeight,)
            ),
             SizedBox(
               height: 10,
             ),

             Text("Davit Warner",style: TextStyle
               (fontSize: 30,color: Colors.black,fontFamily:"Ultra-Regular" ),),
             SizedBox(
               height: 10,
             ),

             Text("Software Engineer",style: TextStyle
               (fontSize: 30,color: Colors.black,),),
             SizedBox(
               height: 10,
             ),
             Container(
               margin: EdgeInsets.symmetric(horizontal: 15),
               // decoration: BoxDecoration( color: Colors.teal, borderRadius: BorderRadius.all(Radius.circular(10))),

               // height: 60,
               // width: 350,
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Image.asset("assrts/images/apple.png", height: 20, width: 20,),
                   SizedBox(width: 10,),
                   Image.asset("assrts/images/facebook.png", height: 20, width: 20,),
                   SizedBox(width: 10,),
                   Image.asset("assrts/images/google.png", height: 20, width: 20,),


                 ],),

             ),
             SizedBox(height: 5,),
             Center(
               child: Text("A Software Engineer is an IT professional who designs, develops and maintains computer software at a company.",textAlign: TextAlign.center,),
             ),
             SizedBox(height: 10,),

             Container(
               margin: EdgeInsets.symmetric(horizontal: 15),
                decoration: BoxDecoration( color: Colors.teal, borderRadius: BorderRadius.all(Radius.circular(10))),

               height: 60,
               width: 350,
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                 children: [
                 Icon(Icons.phone,color: Colors.white,),
                 Text("0171234567", style: TextStyle(color: Colors.white),)
               ],),

             ),
             SizedBox(height: 25,),

             Container(
               margin: EdgeInsets.symmetric(horizontal: 15),
                decoration: BoxDecoration( color: Colors.teal, borderRadius: BorderRadius.all(Radius.circular(10))),

               height: 60,
               width: 350,
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                 children: [
                 Icon(Icons.email,color: Colors.white,),
                 Text("engineer45@gmail.com", style: TextStyle(color: Colors.white),)
               ],),

             ),
             SizedBox(height: 25,),


             GestureDetector(
               onTap: (){
                 print("ok");
               },

               child: Container(
                 height: 30,
                 width: 50,
                 color: Colors.amber,
                 child: Row(
                   children: [
                     Icon(Icons.done),
                     Text("OK",style: TextStyle(fontSize: 15),)
                   ],
                 ),
               ),
             )

          ],

        ),
          
          
          

        ),
      ),
    );
  }
}
